<?php
/*
+---------------------------------------------------------------------------
|   Invision Power Board
|   ========================================
|   by Matthew Mecham
|   (c) 2004 Invision Power Services
|   http://www.invisionpower.com
|   ========================================
+---------------------------------------------------------------------------
|   INVISION POWER BOARD IS NOT FREE SOFTWARE!
|   http://www.invisionboard.com
+---------------------------------------------------------------------------
|
|   > LOG IN MODULE: INTERNAL (IPB AUTH)
|   > Script written by Matt Mecham
|   > Date started: 12:25 Fri. 4th February 2005 (AD)
|
+---------------------------------------------------------------------------
| NOTES:
| This module is part of the authentication suite of modules. It's designed
| to enable different types of authentication.
|
| RETURN CODES
| 'ERROR': Error, check array: $class->auth_errors
| 'NO_USER': No user found in LOCAL record set but auth passed in REMOTE dir
| 'WRONG_AUTH': Wrong password or username
| 'SUCCESS': Success, user and password matched
|
+---------------------------------------------------------------------------
| EXAMPLE USAGE
|
| $class = new login_method();
| $class->is_admin_auth = 0; // Boolean (0,1) Use different queries if desired
|							 // if logging into CP.
| $class->allow_create = 0;
| // $allow_create. Boolean flag (0,1) to tell the module whether its allowed
| // to create a member in the IPS product's database if the user passed authentication
| // but don't exist in the IPS product's database. Optional.
|
| $return_code = $class->authenticate( $username, $plain_text_password );
|
| if ( $return_code == 'SUCCESS' )
| {
|     print $class->member['member_name'];
| }
| else
| {
| 	  print "NO USER";
| }
+---------------------------------------------------------------------------
*/

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded 'admin.php'.";
	exit();
}

class login_method extends login_core
{
	# Globals
	var $ipsclass;
	
	/**
	* Make admin use different auth?
	* @var int
	*/
	var $allow_admin_login = 1;
	
	/**
	* Is admin log in ?
	* @var int
	*/
	var $is_admin_auth     = 0;
	
	var $api_server;


	
	/*-------------------------------------------------------------------------*/
	// Constructor
	/*-------------------------------------------------------------------------*/
	
	function login_method()
	{
	}
	
	/*-------------------------------------------------------------------------*/
	// Authentication
	/*-------------------------------------------------------------------------*/
	
	function authenticate( $username, $password )
	{
    if ( $this->is_admin_auth AND $this->login_method['login_type'] == 'passthrough' )
    {
      // Try local first, so as to not block locally created admins
      $this->admin_auth_local( $username, $password );
      
        if ( $this->return_code == 'SUCCESS' )
        {
          return TRUE;
        }
    }
    
    // Check them against the drupal system
    $response = $this->_do_xml_post( $username, $password );
    
    if( $response == 0 )
    {
      $this->return_code = 'WRONG_AUTH';
      return FALSE;
    }
    else {
      $this->return_code = 'SUCCESS';
      return TRUE;
    }
	}


  //*-------------------------------------
  // User Drupal's built in xml-rpc
  // authentication scheme to authenticate 
  // the user against the Drupal database
  //*--------------------------------------
  function _do_xml_post( $username, $password )
  {
    $id = 0;
   
    // Manually build the xml-rpc request
    $url = $this->login_conf['drupal_xml_rpc_url'];
    $xml_post = "<?xml version=\"1.0\"?>";
    $xml_post .= "<methodCall><methodName>drupal.login</methodName>";
    $xml_post .= "<params><param><value><string>$username</string></value></param>";
    $xml_post .= "<param><value><string>$password</string></value></param>";
    $xml_post .= "</params>";
    $xml_post .= "</methodCall>";

    $params = array(
      'http' => array(
                  'method' => 'POST',
                  'content' => $xml_post
                  )
    );
    
    // Just in case we want to secure things
    // with basic auth to get to the file
    if ( $this->login_conf['http_auth_req'] ) {
      $http_user = $this->login_conf['http_auth_user'];
      $http_pwd  = $this->login_conf['http_auth_pwd'];
      $params['http']['header'] = "Authorization: Basic " . base64_encode("$http_user:$http_pwd");
    }
    
    $ctx = stream_context_create($params);
    $fp = @fopen($url, 'rb', false, $ctx);
    
    if(!$fp) {
      $this->return_code('ERROR');
    }
    
    $response = @stream_get_contents($fp);
    
    /**
     * Drpual returns an int 0 if there's a bad username/pass
     * and string member_uid on success.
     */
    if( $response === false || strpos( $response, 'fault') || strpos( $response, '<int>' )) {
      $this->return_code('ERROR');
    }
    else {
      $look = strpos( $response, '<string>' );
      $id   = $response[$look + 8];
    }
    error_log("Got: $response");
    return $id;

  }
}

?>
