<?php

define('INVISION_AUTH_KEY', variable_get('invision_auth_key', ''));
define('INVISION_OK', 1);
define( 'INVISION_LOGIN_OK', 2);
define('INVISION_LOGIN_ERROR_USER_NOT_FOUND', 3);
define('INVISION_LOGIN_ERROR_BAD_PASSWORD', 8);
define( 'INVISION_LOGIN_ERROR_UNKNOWN', 4);
define( 'INVISION_REGISTER_ERROR', 5);
define( 'INVISION_LOGOUT_ERROR', 6);
define( 'INVISION_UPDATE_ERROR', 7);
define( 'INVISION_USER_UPDATE_OK', 9);
define( 'INVISION_PB_USER_CAT', "Invision PowerBoard");
/**
 * Implementation of hook_help().
 */
function invision_help($section) {
  switch ($section) {
    case 'admin/help#invision':
      $output .= t('Allows you to integrate with the Invision power board.');
      break;
  }
  return $output;
}

function invision_perm() {
  return array(
    'administer invision settings');
}

/**
 * @abstract
 * Implementation of hook_menu
 */
function invision_menu($maycache) {
  $items = array();
  $invision_admin = user_access('administer invision settings');
  $invision_user = user_access('access content');
  
  if($maycache) {
    $items[] = array(
      'path' => 'invision/refresh_user_stats',
      'title' => t('Refresh user InvisionPB stats'),
      'callback' => '_invision_refresh_user_stats',
      'type' => MENU_CALLBACK,
      'access' => $invision_user);
    $items[] = array(
      'path' => 'invision/sso/error',
      'title' => t('Invision SSO Error Handler'),
      'callback' => '_invision_sso_error',
      'type' => MENU_CALLBACK,
      'access' => $invision_user);
    $items[] = array(
      'path' => 'admin/logs/invision',
      'title' => t('Invision Stats'),
      'description' => t('Information about the InvisionPB forums as a whole.'),
      'callback' => '_invision_admin_page',
      'access' => $invision_admin);
    $items[] = array(
      'path' => 'invision/forums',
      'title' => t('Invision forums'),
      'description' => t('View the InvisionPB Forums'),
      'callback' => '_invision_display_forums',
      'access' => $invision_user);
  }
  $items[] = array(
    'path' => 'admin/settings/invision',
    'title' => t(INVISION_PB_USER_CAT),
    'description' => t('Settings for Invision user integration'),
    'callback' => 'drupal_get_form',
    'callback arguments' => array('invision_settings'),
    'access' => $invision_admin);
  $items[] = array(
    'path' => 'admin/settings/invision/create_profiles',
    'title' => t('Re-create profile fields for Invision PowerBoard'),
    'type' => MENU_CALLBACK,
    'callback' => '_invision_create_profile_fields',
    'description' => t('Re-create profile fields for Invision PowerBoard'),
    'access' => $invision_admin);
  return $items;
}

/**
 * The block module for the invision system. 
 * Blocks needed for spec: 
 * TODO The communicatoins block is commented out
 * until we figure out what we are going to do
 * with PM's.
 */
function invision_block($op = 'list', $delta = 0, $edit = array()) {
  if($op == 'list') {
    if(_invision_setup_check()) {
      $block[0]['info'] = t('InvisionPB Latest Posts');
      #$block[1]['info'] = t('InvisionPB Comunications');
      $block[2]['info'] = t('InvisionPD Active Topics');
    }
    return $block;
  }
  else if ($op == 'view') {
    global $user;
    global $invision_user_stats_cache;
    global $base_path;
    // our block content
    $block_content = ' ';
    // last refresh for user
    $refresh_time = $_SESSION['invision_user_refresh'];
    if(!isset($refresh_time) || ( (time() - $refresh_time) >= (int)(variable_get('invision_user_stats_refresh_interval', (60 * 5))))) {
      /* Fetch some user stats */
      _invision_refresh_user_stats(true);
      unset($invision_user_stats_cache);
    }
    
    /**
     * This is cached because we may be implementing 
     * a lot of blocks, and for each block we have (delta),
     * a call to hook_block is made, thus if we have 3 blocks,
     * then 3 calls to the DB to pull user stats will be 
     * made. Since we have large serialized data, it makes 
     * since to only do this once.
     */
    /* User information */
    if(!is_array($invision_user_stats_cache)) {
      $invision_user_stats_cache = array();
      
      $user_stats = db_fetch_object(db_query('SELECT * FROM {invision_user_stats} WHERE uid = %d', $user->uid));
      $invision_user_stats_cache['user_stats'] = $user_stats;
      
      $fstats_rs = db_query('SELECT * FROM {invision_user_forum_stats} WHERE uid = %d', $user->uid);
      $forum_stats = array();
      while($row = db_fetch_object($fstats_rs)) {
        $forum_stats[$row->stat_name] = unserialize(stripslashes($row->stat_value));
      }
      $invision_user_stats_cache['forum_stats'] = $forum_stats;
    }

    // The following line is for debugging purposes.  A link for force refresh    
    $block_content .= l(t('Refresh'), 'invision/refresh_user_stats', array(), drupal_get_destination()) . '<br />'; 
    
    /** 
     * Active posts
     */
    if($delta == 0) {
      $invision_pb_url = variable_get('invision_pb_url', '');
      
      /* Block content */
      if(count($invision_user_stats_cache['forum_stats']['active_posts']) > 0) {
        $block_content .= "<ul>";
        foreach($invision_user_stats_cache['forum_stats']['active_posts'] as $post) {
          $invision_pb_url = variable_get('invision_pb_url', '');
          $mail_letter = theme('image', drupal_get_path('module', 'invision') . '/images/mail_letter.png', 'Go to post', 'Go to post');
          $post_link = "<a href=\"$invision_pb_url?s=&showtopic=" . $post['tid'] . "&view=findpost&p=" . $post['pid'] . "\">$mail_letter</a>";
          $block_content .= "<li><strong><font size=1pt>" . $post['author_name'] . "</font>$post_link</strong></li>";
        }
        $block_content .= "</ul>";  
      }
      else {
        $block_content .= t('(None)');
      }
      
      $block['subject'] =  '<strong><font size=2pt>' . t('InvisionPB Rescent Posts') . '</font></strong>';
      $block['content'] = $block_content;
    }
    /**
     * Active Topics
     * 
     * This block will hold communication information 
     * Perhaps some type of ajax listing of their Invision friends,
     * and if they are online or not, PM's, etc. who knows
     */
    /*
    else if($delta == 1) {
      $block_content .= "<strong>PM</strong>";
      
      if($block_content == '') {
        $block_content .= t('(None)');
      }
      $block['subject'] = '<strong><font size=2pt>' . t('InvisionPB Communcations') . '</font></strong>';
      $block['content'] = $block_content;
    }*/
    else if($delta == 2) {
      if(count($invision_user_stats_cache['forum_stats']['active_topics']) > 0) {
        $invision_pb_url = variable_get('invision_pb_url', '');
        $mail_letter = theme('image', drupal_get_path('module', 'invision') . '/images/mail_letter.png', 'Go to topic', 'Go to topic');
        $block_content .= "<ul>";
        foreach($invision_user_stats_cache['forum_stats']['active_topics'] as $post) {
          $topic_link = "<a href=\"$invision_tp_url?s=&showtopic=" . $post['tid'] . "\">$mail_letter</a>";
          $block_content .= "<li><strong>" . $post['topic_title'] . "</strong> - (" . $post['new_posts'] . ") $topic_link</li>";
        } 
        $block_content .= "</ul>";
      }
      else {
        $block_content .= t('(None)');
      }
      
      $block['subject'] = '<strong><font size=2pt>' . t('InvisionPB Active Topics') . '</font></strong>';
      $block['content'] = $block_content;
    }
    return $block;
  }
}

/**
 * @abstract 
 * Post the data to the specified URL using cURL
 * This is more secure method of posting to the 
 * other site.
 */
function _invision_do_post_request($url, $data = "", $optional_headers = null) {

  // Post to our SSO.php friend across the pond
  # Cookie Jar
  $cookie_jar = file_directory_temp() . '/' . uniqid('invjar') . '.txt';
  $my_cookie_jar = file_directory_temp() . '/' . uniqid('invjar') . '.txt';
  file_put_contents($my_cookie_jar, implode("\n", $_SESSION['invision_cookie_jar']));

  $http_user = '';
  $http_pass = '';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_COOKIEJAR,  $cookie_jar);
  curl_setopt($ch, CURLOPT_COOKIEFILE, $my_cookie_jar);
  curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
  curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REMOTE_ADDR']);
  if($http_user != "") {
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURL_BASIC); // TODO Make this a variable in the settings
    curl_setopt($ch, CURLOPT_USERPWD, "$http_user:$http_pass"); // IF required 
  }
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);  // this line makes it work under https
  $response = curl_exec ($ch);
  curl_close($ch);
  
  $cookies = @file($cookie_jar);
  if(is_array($cookies) && count($cookies) > 0) {
    $_SESSION['invision_cookie_jar'] = $cookies;
    
    foreach($cookies as $cookie) {
      if($cookie[0] == '#' || trim($cookie) == "") {
        continue;
      }
      else {
        $items = explode("\t", str_replace("\n", "", $cookie));
        # Ref: http://www.cookiecentral.com/faq/ (3.5)
        /**
         * Mapping: 
         * 0 - domain
         * 1 - flag if domain can access this variable 
         * 2 - path, the path within the domain that the variable is valid for
         * 3 - secure, if a secure connection with the domain is needed to access variable
         * 4 - expiration
         * 5 - name
         * 6 - value
         */
        @setcookie($items[5], trim(html_entity_decode($items[6])), html_entity_decode($items[4]), $items[2], (bool)(trim($items[3]) === "TRUE"), (bool)(trim($items[1]) === "TRUE"));
      }
    }
    @unlink($cookie_jar);
    @unlink($my_cookie_jar);
  }
  return $response;
} 

/**
 * @abstract The invision admin page
 */
function _invision_admin_page() {
  
  $output = "";
   
  
  $headers = array(t('Stat'), t('Value'));
  
  # Lets theme them as blocks
  $invision_stats = _invision_stats_active();
  
  $rows = array();
  $rows[] = array(t('Members'), $invision_stats->members);
  $rows[] = array(t('Guests'), $invision_stats->guests);
  $rows[] = array(t('Anomymous'), $invision_stats->anon);
  $rows[] = array('<strong>' . t('Total') . '</strong>', $invision_stats->total);
  
  $invision_block = array();
  $invision_block['subject'] = t('Invision User Activity');
  $invision_block['content'] = theme('table', $headers, $rows); 
  
  $output .= theme('block', (object)$invision_block);
  
  # Active Forum members
  $invision_stats = _invision_stats_general();
  
  $rows = array();
  $rows[] = array(t('Total replies'), $invision_stats->total_replies);
  $rows[] = array(t('Total topics'),  $invision_stats->total_topics);
  $rows[] = array(t('Total posts'),   $invision_stats->total_posts);
  $rows[] = array(t('Members'), $invision_stats->mem_count);
  $rows[] = array(t('Most rescent member'), $invision_stats->last_mem_name);
  $rows[] = array(t('Most online members'), $invision_stats->most_count . ' on ' . format_date($invision_stats->most_date));

  $invision_block  = array();
  $invision_block['subject'] = t('Invision Board stats');
  $invision_block['content'] = theme('table', $headers, $rows);

  $output .= theme('block', (object)$invision_block);
  
  return $output;
}

function _invision_stats_active() {
  $ssi_url = variable_get('invision_ssi_url', '');
  $act = "?a=active&out=xml";
  
  $response = _invision_do_post_request($ssi_url . $act);
  
  $invision_obj = @simplexml_load_string($response);
  
  return $invision_obj; 
}


function _invision_stats_general() {
  $ssi_url = variable_get('invision_ssi_url', '');
  $act = "?a=stats&out=xml";

  $response = _invision_do_post_request($ssi_url . $act);

  $invision_obj = @simplexml_load_string($response);
  
  return $invision_obj; 
}

/**
 * Implementation of hook_auth
 * @abstract The auth hook takes care of a 
 * very simple task. It checks to see if 
 * the username is in the database. If they are 
 * not, it creates the user if, and only if the user
 * is authenticated from Invision.
 */
function invision_auth($username, $password, $server = "") {
  // Check the db
  $check_user = db_result(db_query("SELECT 1 FROM {users} WHERE name = '%s'", $username));
  if($check_user != 1) {
    $account = new stdClass();
    $account->name = $username;
    
    // Since this user doesn't exist, we need to supply the salt
    // for authentication
    $params = array();
    $params['u'] = $username;
    $params['op'] = 'user_info';
    $params['auth'] = INVISION_AUTH_KEY;
    $dest_url = variable_get('invision_sso_url', '');
    $info = _invision_do_post_request($dest_url, $params);
    $info = @unserialize($info);
    $info = $info['user_info'];
    $account->invision_salt = $info['converge_pass_salt'];
    $account->pass = $password;
    // Re-auth using the salt
    $params['auth_user'] = 1; 
    $params['p'] = _invision_hash($info['converge_pass_salt'], md5($password));
    $user_check = _invision_do_post_request($dest_url, $params);
    if($user_check == INVISION_LOGIN_ERROR_BAD_PASSWORD) {
      return;
    }
    $user_check = @unserialize($user_check);
    if(is_array($user_check) && count($user_check) > 0)  {
     return true;
    }
    else {
      return;
    }
  }
  return;
}

function _invision_do_user_first_pull(&$account, $auth_user = 0) {
  // Do the first pull fo the user
  $dest_url = variable_get('invision_sso_url', '');

  // Pull the information about the user from 
  // invision, and set it to Drupal, then save the face 
  // that this happend.
  $params = array();
  $params['u'] = $account->name;
  $params['op'] = 'user_info';
  $params['p'] = _invision_hash($account->invision_salt, $account->pass, isset($account->uid) );
  $params['auth_user'] = $auth_user === true ? 1 : 0; // Tells Invision to auth user using name/pass first
  $params['auth'] = INVISION_AUTH_KEY;
  
  $user_fetch = _invision_do_post_request($dest_url, $params);

  if( $user_fetch == INVISION_LOGIN_ERROR_USER_NOT_FOUND || $user_fetch == INVISION_LOGIN_ERROR_BAD_PASSWORD ) {
    return $user_fetch;
  }
  $user_fetch = unserialize($user_fetch);
  $user_info = $user_fetch['user_info'];
  
  // Set the infor we just pulled
  $invision_user_info = array();
  $drupal_profile_info = array();
  
  $profile_mappings = _invision_profile_map();
  foreach($profile_mappings as $name => $map_info) {
    $drupal_profile_info[$map_info['field_name']] = $user_info[$map_info['db_name']];
    if(isset($map_info['field_name'])) {
      $account->$map_info['field_name'] = $user_info[$map_info['db_name']];
    }
  }
  # Have to set the birthday manually
  $account->$profile_mappings['birthday']['field_name'] = $user_info['bday_month'] . '-' . $user_info['bday_day'] . '-' . $user_info['bday_year'];
  $drupal_profile_info[$profile_mappings['birthday']['field_name']] = $user_info['bday_month'] . '-' . $user_info['bday_day'] . '-' . $user_info['bday_year'];
  
  # Save the profile
  profile_save_profile($drupal_profile_info, $account, INVISION_PB_USER_CAT);
  
  # Save the salt explicityly
  user_save($account, array('invision_first_pull' => TRUE, 'invision_salt' => $user_info['converge_pass_salt']), 'DONOTMATCH');
  
  $account->invision_salt = $user_info['converge_pass_salt'];
  return INVISION_OK;
}


/**
 * @abstract Authentication override for invision.
 * This is where we will auth the user to the invision 
 * power board system after they are authenticated to 
 * the drupal system. 
 */
function invision_user($op, &$edit, &$account, $category= NULL) {
  
  if(!_invision_setup_check()) {
    watchdog('invision', t('Invision authorization cancled becuase your settings have not been checked. Please check your settings on the administration page.'));
    return;
  }
  $username = $account->name;
  $password = $account->pass;
  
  switch($op) {
    case 'login':
      $dest_url = variable_get('invision_sso_url', '');
      $current_dest = explode('=', drupal_get_destination());
      $destination = $current_dest[1];
      $return_url = url($destination, NULL, NULL, TRUE);
      /**
       * Initial Pull
       */
      # First pull section, runs if its a sucessful login.
      if(!isset($account->invision_first_pull)) {
        // Pull the information about the user from 
        // invision, and set it to Drupal, then save the face 
        // that this happend.
        _invision_do_user_first_pull($account);
      }

      $params = array();
      $params['op'] = 'login';
      $params['u'] = $username;
      $params['p'] = _invision_hash($account->invision_salt, $password);
      $params['url'] = $return_url;
      $params['response_type'] = 'string';
      $query_string_params = _invision_url_params($params);
      
      $response = _invision_do_post_request($dest_url, $query_string_params);

      /**
       * If the user was not authenticated, we 
       * have options. 
       * 1. If it a bad password,
       * we need to just resync with the 
       * drupal password.
       * 2. User not found, register (if allowable)
       * 3. Unknown, alert administrator
       */
      if( $response != INVISION_LOGIN_OK ) {
        
        $auto_register = variable_get('invision_auto_register', 1);
        
        if( $auto_register ) {
        
          if( $response == INVISION_LOGIN_ERROR_BAD_PASSWORD ) {
            
            # Update user
            _invision_sync_user($username, $password, $account, 'update', 'account');
            $edit['invision_salt'] = $account->invision_salt;
            user_save($account, array('invision_salt' => $account->invision_salt)); // Save the salt
            
            # Now login
            $params['p'] = _invision_hash($account->invision_salt, $password); // Password is already md5 hashed
            $query_string_params = _invision_url_params($params);
            _invision_do_post_request($dest_url, $params);
        
          }
          else if ( $response == INVISION_LOGIN_ERROR_USER_NOT_FOUND ) {
        
            # Register the user
            _invision_sync_user($username, $password, $account, 'insert', '', (array)$account);
            
            # watchdog msg
            watchdog('invision', t('%username was automatically registered to the InvisionPB system.', array('%username' => $username)));          
            
            # Now login
            $params['p'] = _invision_hash($account->invision_salt, $password); // Password is already md5 hashed
            $query_string_params = _invision_url_params($params);
            
            _invision_do_post_request($dest_url, $query_string_params);
        
          }
        }
        else {
          
          watchdog( 'invision', t('Unkown error during Invision login for %username. <br />' .
              'Return code: %return_code', array('%username' => $username, '%return_code' => $response)));
        }
      }
      

      
      
      break;
    case 'logout':
      $dest_url = variable_get('invision_sso_url', '');
      /* Please take me to the front page sir */
      $return_url = url('', NULL, NULL, TRUE);
      $params = array();
      $params['op'] = 'logout';
      $params['u'] = $username;
      $params['p'] = $password; 
      $params['url'] = $return_url;
      $params = _invision_url_params($params);
      
      $response = _invision_do_post_request($dest_url, $params);
      
      break;
    case 'submit':
      # Salt is set in submit before 
      # update so that it always stays 
      # behine the scenes.
      $edit['invision_salt'] = _invision_generate_salt();
      break;
    case 'update':
    case 'insert':
      if($category == 'account' || $category == t(INVISION_PB_USER_CAT)) {
        if($edit['invision_salt'] == "") {
          $edit['invision_salt'] = $account->invision_salt; // For a progrmatically created user
        }
        _invision_sync_user($username, $password, $account, $op, $category, $edit, $params);
      }
      break;
    case 'view':
      
      break;
  }
  
  // TODO Set to false for testing
  return FALSE; // Always return true cuase we just want to auth them to invision
}

/**
 * @abstract Takes a user name, and looks up their 
 * profile information and pushes the 
 * information to invision.
 * 
 * @author Earnest Berry III <earnest.berry@gmail.com>
 * @param $username string Username
 * @param $password string Password
 * @param $account  object User account object
 * @param $op string operatoin
 * @param $edit array post edit params
 * @param $params array params
 * 
 * @return boolean Boolean value telling if the user was synced or not
 * 
 */
function _invision_sync_user($username, $password, &$account = NULL, $op, $category = "", $edit = array(), $params = array()) {
  // Save this salt to the remote user
  /* The setup */
  $dest_url = variable_get('invision_sso_url', '');
  $current_dest = explode('=', drupal_get_destination());
  $destination = $current_dest[1];
  $return_url = url($destination, NULL, NULL, TRUE);
  
  /* The hook */
  $params = array();
  $params['url'] = $return_url;
  
  if($op == 'insert') {
    $op = 'register';
    
    $edit['invision_salt'] = _invision_generate_salt();
    foreach(_invision_profile_map() as $invision_profile => $drupal_profile) {
      $params[$invision_profile] = $edit[$drupal_profile['field_name']];        
    }
    $params['username'] = $username; 
    $params['password'] = _invision_hash($edit['invision_salt'], $password);
    $params['joindate'] = $edit['created'] ? $edit['created'] : time();
    $params['email'] = $edit['mail'];
  }
  else {
    // If we're not on the Invision PowerB category, we need to use the account info
    if($category == t( INVISION_PB_USER_CAT )) {
      foreach(_invision_profile_map() as $invision_profile => $drupal_profile) {
        $params[$invision_profile] = $edit[$drupal_profile['field_name']];  
        $params['name'] = $username; // Important, this is how it updates information
      }
    }
    else if ($category == 'account'){
      # If we are under the account tab, we have to send all informatoin to 
      # sync so that we can resync a password change 
      foreach(_invision_profile_map() as $invision_profile => $drupal_profile) {
        $params[$invision_profile] = $account->$drupal_profile['field_name'];  
        $params['name'] = $username; // Important, this is how it updates information
      }
    }
    else {
      return;
    }
  }

  /* Overwrite the bloody account salt */
  $params['salt'] = $edit['invision_salt'] ? $edit['invision_salt'] : _invision_generate_salt();
  $params['password'] = md5(md5($params['salt']) . $password); // Password is already md5 hashed Note: could hash again to totally break/control invision login system
  $account->invision_salt = $params['salt'];
  
  /* Birthday blow out */
  if($edit['birthday'] == "") {
    $params['birthday'] = $params['birthday']['month'] . '-' . $params['birthday']['day'] . '-' . $params['birthday']['year'];
  }
  
  /* Timezone conversion */
  if($edit['timezone']) {
    $params['time_offset'] = (int)($edit['timezone']/60/60);
  }
  $params['op'] = $op;
  
  /* Go gadget go! */
  $params = _invision_url_params($params);
  $response = _invision_do_post_request($dest_url, $params);
  // TODO For now, these are the only 2 responses. Why not unify the responses into just "OK" or "ERROR", or a custom xml handler to parse returned errors?
  if($response != INVISION_USER_UPDATE_OK && $response != INVISION_OK) {
    #drupal_set_message(t('There was an error syncing your information to the Invision system. Please try re-saving your ' .
    #  'information, or contact an administrator.'), 'error');
    watchdog('invision', t('Error while syncing information for %user_name', array('%user_name' => $username)));
  }
  else {
    #drupal_set_message(t('Invision information updated successfully.'));
  }
  
  return;
}


/**
 * @abstract This function handles keeping 
 * the users basic information, salt, and 
 * passowrds in sync.
 */



function _invision_url_params($params) {
  $url_params = array();
  foreach($params as $k => $v) {
      if($v == '') {
        $v = ' '; // This is here because you get a driver error with a completly empty string. It seems to be ok with spaces.
      }
      $url_params[] = drupal_urlencode($k) . "=" . drupal_urlencode($v);
  }
  $url_params[] = 'error_url=' . url('invision/sso/error', NULL, NULL, TRUE);
  $url_params[] = 'auth=' . INVISION_AUTH_KEY;
  
  return implode('&', $url_params);
}

function _invision_generate_salt() {
  // Salt Generator (FROM IPB)
  $salt = '';
  $len=5;
  srand( (double)microtime() * 1000000 );
  for ( $i = 0; $i < $len; $i++ ) {
      $num   = rand(33, 126);
      
      if ( $num == '92' ) {
          $num = 93;
      }
      $salt .= chr( $num );
  }
  return $salt;
}


/**
 * Catch the saving of a user and the updating/saving 
 * of a profile.
 */
function invision_nodeapi(&$node, $ip, $arg3 = NULL, $arg4 = NULL) {
  return;
}

/**
 * @abstract Updates the stats of the user.
 */
function _invision_refresh_user_stats($background = false) {
  global $user;
  $_SESSION['invision_user_refresh'] = time();
  $dest_url = variable_get('invision_sso_url', '');
  $current_dest = explode('=', urldecode(drupal_get_destination()));
  $destination = $current_dest[1];
  $return_url = url($destination, NULL, NULL, TRUE);
  
  $params['op'] = 'user_stats';
  $params['u'] = $user->name;
  $params['url'] = $destination;
  $params['auth'] = INVISION_AUTH_KEY;
  #$params['lastdate'] = (60 * 60 * 24 * 30); // The range we want
  
  # Go Gadget Go!
  $params = _invision_url_params($params);
  $response = _invision_do_post_request("$dest_url?$params");
  $response = unserialize($response);
  
  # Parse the reponse
  
  if(is_array($response)) {
    db_query('SET AUTOCOMMIT=0');
    db_query('START TRANSACTION'); // In case someone actually uses transaction safe tables, *GASP!*
    db_query("DELETE FROM {invision_user_stats} WHERE uid = %d", $user->uid);
    $try = true;
    foreach($response['member'] as $stat_name => $stat_value) {
      $try = db_query("INSERT INTO {invision_user_stats}(stat_id, uid, stat_name, stat_value) " .
          "VALUES(%d, %d, '%s', '%s')", db_next_id('{invision_user_stats}_stat_id'), $user->uid, $stat_name, $stat_value);
      if(!$try) {
        break;
      }
    }
    // TODO Think about moving this to a seperate table 
    // so that you can search the user table more readily because 
    // the serailized arrays are pretty big
    # Update the user general forum stats
    $stat_name = 'active_topics';
    $stat_value = $response['active_topics'];
    $try1 = db_query("DELETE FROM {invision_user_forum_stats} WHERE uid = %d", $user->uid);
    $try2 = db_query("INSERT INTO {invision_user_forum_stats}(fstid, uid, stat_name, stat_value) " .
        "VALUES(%d, %d, '%s', '%s')", db_next_id('{invision_user_forum_stats}_fstid'), $user->uid, $stat_name, addslashes(serialize($stat_value)));
    # Rescent Posts
    $stat_name = 'active_posts';
    $stat_value = $response['active_posts'];
    $try3 = db_query("INSERT INTO {invision_user_forum_stats}(fstid, uid, stat_name, stat_value) " .
        "VALUES(%d, %d, '%s', '%s')", db_next_id('{invision_user_forum_stats}_fstid'), $user->uid, $stat_name, addslashes(serialize($stat_value)));
    $try = $try1 && $try2 && $try3 && $try;
    
    # Transaction handeling
    if(!$try) {
      db_query('ROLLBACK');
      watchdog('invision', t('Error updating user stats for user %username', array('%username' => $user->name)), WATCHDOG_ERROR);
    }
    else {
      db_query('COMMIT');
    }
  }
  else {
    watchdog('invision', t('No response from Invision when updating stats for user %username', array('%username' => $user->name)), WATCHDOG_ERROR);
  }
  db_query('SET AUTOCOMMIT=1');
  drupal_goto($destination);
  return;
}




function invision_settings() {
  $form = array();
  
  drupal_set_message(l(t('Click here to recreate profiles'), 'admin/settings/invision/create_profiles'));
  
  $form['invision_pb_url'] = array(
    '#type' => 'textfield',
    '#title' => t('InvisionPB Url'),
    '#default_value' => variable_get('invision_pb_url', ''),
    '#description' => t('The full url path to your InvisionPB site. Include the index.php. E.g. <i>http://myboard.com/index.php</i>'));
  $form['invision_show_inline'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show forums inline as content'),
    '#description' => t('Check this box to show the content inline as any other content. This means 
    that there will be blocks on the side, etc. If this box is unchecked, the forums will show and 
    no blocks will be shown; meaning the forums will take up the full width of the browser.'),
    '#default_value' => variable_get('invision_show_inline', FALSE));
  $form['invision_sso_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Invision PB SSO Url'),
    '#default_value' => variable_get('invision_sso_url', ''));
  $form['invision_ssi_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Invision SSI Url'),
    '#default_value' => variable_get('invision_ssi_url', ''));
  $form['invision_surpress_messages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Surpress user status message'),
    '#default_value' => variable_get('invision_surpress_messages', FALSE),
    '#description' => t('Check this box if you wish the user not to see status messages. Example, if you want the 
    user to see a message that his profile was updated ok, or that it was not updated ok, check this box. Else, 
    all status messages will be recorded in watchdog. Regardless as to if this box is checked or not, all messages 
    are recorded in watchdog.'));
  $form['invision_auto_register'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto-register'),
    '#default_value' => variable_get('invision_auto_register', 1),
    '#description' => t('Check this box to have a user automatically registerd into the InvisionPB system ' .
        'if, AFTER they have been authenticated by Drupal, they are not found in the ' .
        'Invision system. It is usually a good idea to have this box checked.'));
  $form['invision_auth_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Invision authorization key'),
    '#default_value' => variable_get('invision_auth_key', ''),
    '#description' => t('This key must be set here and on your remote Invision PowerBoard site in the ' .
        'SSO.php file. This key helps secure both end points so that they can authenticate who ' .
        'the request came from. This key can be arbitrarily long.'));
  $form['invision_user_stats_refresh_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('InvisionPB user stats refresh interval'),
    '#description' => t('Interval to automatically refresh user data. This ' .
        'happends on a per user basis while they are logged in ' .
        'only. NOT in a batch.'),
    '#default_value' => variable_get('invision_user_stats_refresh_interval', 60 * 5));
  $form['invision_session_check_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('The intervalr to check the users session.'),
    '#default_value' => variable_get('invision_session_check_interval', (60 * 5)));
    
  
  return system_settings_form($form);
}



function _invision_load_sso() {
  define('DRUPAL_SET_INVISION_ROOT_PATH', variable_get('invision_path', ''));
  $sso_path = drupal_get_path('module', 'invision') . "/SSO.php";
  require_once($sso_path);
}

/**
 * Returns mapping information 
 * of the invision profile information 
 * to the user profile information
 * 
 * If the type is set to NULL, a profile is 
 * !NOT! created for it.
 */
function _invision_profile_map() {
  $map = array(
    'name' => array(
      'field_name' => 'name',
      'db_name' => 'name', 
      'type' => NULL),
    'password' => array(
      'field_name' => 'pass',
      'db_name' => 'pass', 
      'type' => NULL),
    'time_offset' => array(
      'field_name' => 'timezone',
      'type' => NULL),
    'homepage' => array(
      'field_name' => 'profile_invision_homepage',
      'db_name' => 'homepage', 
      'type' => 'url', 
      'reg'  => true,
      'title' => t('Personal Homepage')),
    'salt' => array(
      'field_name' => 'invision_salt', 
      'type' => NULL),
    'aim_name' => array(
      'field_name' => 'profile_invision_aim',
      'title' => t('AIM screen name'),
      'db_name' => 'aim_name', 
      'type' => 'textfield'),
    'icq_number' => array(
      'field_name' => 'profile_invision_icq', 
      'title' => t('ICQ number'),
      'db_name' => 'icq_number',
      'type' => 'textfield'),
    'yahoo' => array(
      'field_name' => 'profile_invision_yahoo', 
      'title' => t('Yahoo! messenger'),
      'db_name' => 'yahoo',
      'type' => 'textfield'),
    'msnname' => array(
      'field_name' => 'profile_invision_msn', 
      'title' => t('MSN Name'),
      'db_name' => 'msnname',
      'type' => 'textfield'),
    'birthday' => array(
      'field_name' => 'profile_invision_birthday',
      'title' => t('Birthday'),
      'type' => 'date',
      'req' => true,
      'reg' => true),
    'members_display_name' => array(
      'field_name' => 'profile_invision_members_display_name',
      'title' => t('Display name'),
      'type' => 'textfield',
      'db_name' => 'members_l_display_name',
      'req' => true,
      'reg' => true)
    );
  return $map;
}

/**
 * @abstract Create the neccessary 
 * profile fields to keep in sync 
 * with Invision Power Board
 */

function _invision_create_profile_fields() {
  /* Start your engines */
  # Set the type to NULL to have the field NOT be created
  global $user;
  $map = _invision_profile_map();
  /* On your mark */
  $field_count = 0;
  $form_values = array();
  $category = t('Invision PowerBoard'); // TODO Will language translatoin make this quirky?
  
  /* Get set */
  $current_fields_rs = _profile_get_fields($category);
  while($row = db_fetch_object($current_fields_rs)) {
    $current_fields[$row->name] = $row->name;
  }
  
  foreach($map as $invision_name => $drupal_profile) {
    /* Create profiles */
    if($drupal_profile['type'] !== NULL && !isset($current_fields[$drupal_profile['field_name']])) {
      $form_values = array();
      $form_values['category'] = $category;
      $form_values['title'] = $drupal_profile['title'];
      $form_values['name'] = $drupal_profile['field_name'];
      $form_values['explanation'] = '';
      $form_values['visibility'] = PROFILE_PUBLIC_LISTINGS;
      $form_values['required'] = $drupal_profile['req'];
      $form_values['register'] = $drupal_profile['reg'];
      $form = drupal_execute('profile_field_form', $form_values, $drupal_profile['type']);
      ++$field_count;
    }
  }
  
  drupal_set_message(t('Created %field_count field(s)', array('%field_count' => $field_count)));
  drupal_goto('admin/settings/invision');
  return;
}

/**
 * Todo, check the setup of the invision 
 * module
 */
function _invision_setup_check() {
  return true;
}

/**
 * @abstract Centralizing the hashing of the 
 * password to invision standards.
 * 
 * @author Earnest Berry III <earnest.berry@gmail.com>
 * 
 * @param $salt string Salt
 * @param $password string password
 * @param $password_prehashed boolean If the passowrd is hashed already
 * 
 * @return string Hashed password
 */
function _invision_hash($salt, $password, $password_prehashed = TRUE) {
  if( !$password_prehashed ) {
    $password = md5($password);
  }
  return md5( md5($salt) . $password);
}


/**
 * @abstract Implementation of hook_exit
 * Checks to see if the Invision session is still alive 
 * onlye if the user is logged in and the user 
 * is not an anonymous user.
 * If the session is found to be dead, it 
 * relogs the user back into the Invision board.
 */
function invision_init($destination = NULL) {
  global $user;

  /**
   * This section takes care of a COOKIE header problem.
   * If we are in the middle of drupal, say in our module,
   * etc., something well after the bootstrap, sometimes 
   * the cookie headers are sent to late and are ignored.
   * 
   * If we do this during the init, this 
   * shouldn't be a problem.
   */
  if(  $user->uid == 0 || $user->uid == "" ) {
    return;
  }
  else if(variable_get('invision_session_check_interval', (60 * 5)) > time() - $_SESSION['invision_last_session_check'] || $_COOKIE['member_id'] == 0 || !(isset($_COOKIE['member_id'])) ) {
    if( !(isset($_COOKIE['member_id'])) || $_COOKIE['member_id'] == 0 ) {

        $dest_url = variable_get('invision_sso_url', '');
        $params = array();
        $params['op'] = 'login';
        $params['u'] = $user->name;
        $params['p'] = _invision_hash($user->invision_salt, $user->pass);
        $params['url'] = ""; // Used for when we were boinking...deprecated
        $params['response_type'] = 'string';
        $query_string_params = _invision_url_params($params);
        $response = _invision_do_post_request($dest_url, $query_string_params);
        
        // If it's a bad password, sync and resubmit
        if($response == INVISION_LOGIN_ERROR_BAD_PASSWORD) {
          return;
          // Considered a re-sync in midst of session as 
          // outlined below.
          /*
          _invision_sync_user($user->name, $user->pass, $user, 'update', 'account');
          user_save($user, array('invision_salt' => $user->invision_salt)); // Save the new salt
          $params['p'] =_invision_hash($user->invision_salt, $user->pass);
          $response = _invision_do_post_request($dest_url, $query_string_params);
          */
        }
        
    }
    $_SESSION['invision_last_session_check'] = time();
  }
}

function _invision_display_forums() {
  $url = variable_get('invision_pb_url', '');
  $output = "";
  $path_to_js = drupal_get_path('module', 'invision') . '/autosize.js';
  drupal_add_js($path_to_js, 'module', 'header', FALSE, FALSE);
  
  $output = '<iframe id="myframe" src="' . $url . '" width="100%" height="100%" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" />';  
  /**
   * If we are not to show blocks or anything lese
   * we will print out own page.
   */
  
  if ( variable_get('invision_show_inline', FALSE ) ) {
    return $output;
  }
  else {
    print theme('page', $output, FALSE);
    return;
  }
}


