<?php
define( 'IPB_THIS_SCRIPT', '0');
/* General init */
require_once( './init.php' );
$INFO = array();

define( 'AUTH_KEY', '075791cabda06e9e90966c9c4f7cf5f9' );
define('INVISION_OK', 1);
define( 'INVISION_LOGIN_OK', 2);
define('INVISION_LOGIN_ERROR_USER_NOT_FOUND', 3);
define('INVISION_LOGIN_ERROR_BAD_PASSWORD', 8);
define( 'INVISION_LOGIN_ERROR_UNKNOWN', 4);
define( 'INVISION_REGISTER_ERROR', 5);
define( 'INVISION_LOGOUT_ERROR', 6);
define( 'INVISION_UPDATE_ERROR', 7);
define( 'INVISION_USER_UPDATE_OK', 9);


/* Load Classes */
require_once ROOT_PATH   . "sources/ipsclass.php";
require_once ROOT_PATH   . "sources/classes/class_display.php";
require_once ROOT_PATH   . "sources/classes/class_session.php";
require_once ROOT_PATH   . "sources/classes/class_forums.php";
require_once KERNEL_PATH . "class_converge.php";
require_once ROOT_PATH   . "conf_global.php";

/* Setup ipsclass */
$ipsclass       = new ipsclass();
$ipsclass->vars = $INFO;
$ipsclass->init_db_connection();
$ipsclass->parse_incoming();
$ipsclass->converge = new class_converge( $ipsclass->DB );
$ipsclass->cache_array = array( 'settings' );
$ipsclass->init_load_cache( $ipsclass->cache_array );
$ipsclass->initiate_ipsclass();

/* Post Handler */
$handler = new post_handler();
$handler->ipsclass =& $ipsclass;

/* Switch on op */
switch( $ipsclass->input['op'] )
{
	case 'user_exists':
    $handler->user_exists();
    break;
  case 'user_stats':
    $handler->user_stats();
    break;
  case 'user_info':
    $handler->user_info();
  case 'server_stats':
    $handler->server_stats();
    break;
  case 'register':
		$handler->register_user();
	break;

	case 'update':
		$handler->update_user();
	break;
	
  case 'logout':
    $handler->logout_user();
    break;
	case 'login':
		$handler->login_user();
  default:
	break;
}

/**
 * Post Handler Class
 *
 * Class for handling the post
 *
 * Global Post Parameters
 *
 *  - string response_type If left blank a simple 1 or 0 is returned
 *                         If set to xml, more detailed errors are sent via an xml document
 *  - string auth          Authentication Key, required for the register and update methods  
 * 
 * @author    Joshua Williams <josh@invisionpower.com>
 * @version   1.0
 * @since     1.0
 * @access    private
 * @copyright Invision Power Inc.
 */ 
class post_handler
{
	/**
	 * post_handler::authenticate()
	 * 
	 * Checks to see if the auth key matches, returns error if yes
	 *
	 * @access private
	 * @return void
	 **/	
	function authenticate()
	{
		/* Auth Check */
		if( AUTH_KEY != $this->ipsclass->input['auth'] )
		{
			$this->return_error( array( 'Incorrect Authorization Key' ) );	
		}		
    // TODO Auth via the HTTP-REF also. If something goes wrong, the auth key is in plain site
	}
	
  /**
   * post_handler::logout_user()
   * 
   * Logs the user out of the boards system
   */
  function logout_user() {

    
    /* Remove Cookies */
    # Taken from login.php::delete_cookies
    if (is_array($_COOKIE))
    {
      foreach( $_COOKIE as $cookie => $value)
      {
        if (preg_match( "/^(".$this->ipsclass->vars['cookie_id']."ipbforum.*$)/i", $cookie, $match))
        {
          $this->ipsclass->my_setcookie( str_replace( $this->ipsclass->vars['cookie_id'], "", $match[0] ) , '-', -1 );
        }
      }
    }
    
    $this->ipsclass->my_setcookie('pass_hash' , '-1');
    $this->ipsclass->my_setcookie('member_id' , '-1');
    $this->ipsclass->my_setcookie('session_id', '-1');
    $this->ipsclass->my_setcookie('topicsread', '-1');
    $this->ipsclass->my_setcookie('anonlogin' , '-1');
    $this->ipsclass->my_setcookie('forum_read', '-1');
  }
   
	/**
	 * post_handler::login_user()
	 * 
	 * Logs the specified user into the forum
	 *
	 * On Success: User is sent to the url specified in the post string
	 * On Failure: Error is returned
	 *
	 * The following fields must be passed through the POST:
	 *
	 * - string u
	 * - string p Must be in the format md5( md5( salt ) . md5( password ) )
	 * - string url The user will be bounced to this url on a successful login
	 * 
	 * @access public
	 * @return xml
	 **/	
	function login_user()
	{
		/* Setup */
  	$url    = "";
  	$member = array();
  	
  	$user = $this->ipsclass->input['u'];
  	$pass = $this->ipsclass->input['p'];
  	$url  = $this->ipsclass->input['url'];
    
		/* Error Checking */    	
    	if( ! $user )
    	{
    		$errors[] = $this->ipsclass->lang['no_username'];
    	}
    
    	if( ! $pass )
     	{
    		$errors[] = $this->ipsclass->lang['pass_blank'];
    	}   
    	
    	if( ! $url )
     	{
    		$errors[] = 'Return URL was not specified';
    	}       	
		
    	/* Query Member */
    $this->ipsclass->DB->cache_add_query( 'login_getmember', array( 'username' => $user ) );
		$this->ipsclass->DB->cache_exec_query();			
		$this->member = $this->ipsclass->DB->fetch_row();

		/* Found user? */			
		if ( ! $this->member['id'] )
		{
    		$this->return_error( INVISION_LOGIN_ERROR_USER_NOT_FOUND );
		}
		
		/* Check Converge */
		$this->ipsclass->converge->converge_load_member( $this->member['email'] );			
		if ( ! $this->ipsclass->converge->member['converge_id'] )
		{
    		$this->return_error( INVISION_LOGIN_ERROR_USER_NOT_FOUND );
		}
    
		/* Check Password */
		if( $this->ipsclass->converge->member['converge_pass_hash'] != $pass )
		{  
    		$this->return_error( INVISION_LOGIN_ERROR_BAD_PASSWORD );
		}
		
		if( $errors )
		{
			// Boink them back to the url with appended information. TODO Add some information to the url params to let drupal know there was an error
      $this->return_error( $errors );	
		}	
    
		/* Set Cookies */
		$this->ipsclass->my_setcookie( "member_id"   , $this->member['id']              , 1);
		$this->ipsclass->my_setcookie( "pass_hash"   , $this->member['member_login_key'], 1);

		/* Update IP Address */	
		if ( $this->member['ip_address'] == "" OR $this->member['ip_address'] == '127.0.0.1' )
		{
			$this->ipsclass->DB->simple_construct( array( 'update' => 'members',
														  'set'    => "ip_address='{$this->ipsclass->ip_address}'",
														  'where'  => "id={$this->member['id']}"
												 )      );
								 
			$this->ipsclass->DB->simple_exec();
		}
		
		//-----------------------------------------
		// Create / Update session
		//-----------------------------------------
		
		$poss_session_id = "";
		
		if ( $cookie_id = $this->ipsclass->my_getcookie('session_id') )
		{
			$poss_session_id = $this->ipsclass->my_getcookie('session_id');
		}
		else if ( $this->ipsclass->input['s'] )
		{
			$poss_session_id = $this->ipsclass->input['s'];
		}
		
		//-----------------------------------------
		// Clean...
		//-----------------------------------------
		
		$poss_session_id = preg_replace("/([^a-zA-Z0-9])/", "", $poss_session_id);
		
		if ($poss_session_id)
		{
			$session_id = $poss_session_id;
			
			//-----------------------------------------
			// Delete any old sessions with this users IP
			// addy that doesn't match our session ID.
			//-----------------------------------------
			
			$this->ipsclass->DB->simple_construct( array( 'delete' => 'sessions',
														  'where'  => "ip_address='".$this->ipsclass->ip_address."' AND id <> '$session_id'"
												 )      );
								 
			$this->ipsclass->DB->simple_shutdown_exec();
			
			
			$this->ipsclass->DB->do_shutdown_update( 'sessions',
													 array (
															 'member_name'  => $this->member['members_display_name'],
															 'member_id'    => $this->member['id'],
															 'running_time' => time(),
															 'member_group' => $this->member['mgroup'],
															 'login_type'   => 0,
														   ),
													 "id='".$session_id."'"
												 );
		}
		else
		{
			$session_id = md5( uniqid(microtime()) );
			
			//-----------------------------------------
			// Delete any old sessions with this users IP addy.
			//-----------------------------------------
			
			$this->ipsclass->DB->simple_construct( array( 'delete' => 'sessions',
														  'where'  => "ip_address='".$this->ipsclass->ip_address."'"
												 )      );
								 
			$this->ipsclass->DB->simple_shutdown_exec();
			
			$this->ipsclass->DB->do_shutdown_insert( 'sessions',
													 array (
															 'id'           => $session_id,
															 'member_name'  => $this->member['members_display_name'],
															 'member_id'    => $this->member['id'],
															 'running_time' => time(),
															 'member_group' => $this->member['mgroup'],
															 'ip_address'   => $this->ipsclass->ip_address,
															 'browser'      => substr($this->ipsclass->clean_value($_SERVER['HTTP_USER_AGENT']), 0, 50),
															 'login_type'   => 0,
												  )       );
		}
		
		$this->ipsclass->member           = $member;
		$this->ipsclass->session_id       = $session_id;
    
		/* All Done */
		$this->ipsclass->my_setcookie("session_id", $this->ipsclass->session_id, -1);
		$this->logged_in = 1;

    $this->return_ok( INVISION_LOGIN_OK );
	}
  
  
  /**
   * post_handler::user_exists()
   * @abstract - Simple function 
   * to test for the existance of 
   * a user.
   */
  function user_exists() {
    /* Authenticate Request */
    $this->authenticate();
    error_log("Checking for user: " . $this->ipsclass->input['u']);
    $username = $this->ipsclass->input['u'];
    // Check the user
    $this->ipsclass->DB->simple_construct(
      array(
        'select' => '1 as found',
        'from' => 'members',
        'where' => "name = '$username'"));
    $this->ipsclass->DB->exec_query();
    $row = $this->ipsclass->DB->fetch_row();
    if(isset($row['found']) && $row['found'] == 1) {
      $this->return_ok($row['found']);
    }
    else {
      $this->return_ok(-1);
    }
  }
	
	/**
	 * post_handler::update_user()
	 * 
	 * Registers a new user in the local IPB database
	 *
	 * The following fields must be passed through the POST:
	 *
	 * - string name Can not be updated
	 * 
	 * The following fields can be sent for update
	 *
	 * - string homepage
	 * - string icq
	 * - string aim
	 * - string yahoo
	 * - string birthday Must be in the format M-D-YYYY
	 * - string password Must be in the format md5( md5( salt ) . md5( password ) )
	 * - string hash     Must be less than 6 characters
	 *
	 * @access public
	 * @return xml
	 **/		
	function update_user()
	{
		/* Authenticate Request */
		$this->authenticate();
		
		/* Get the post input */
		$name     = $this->ipsclass->input['name'];
		$password = $this->ipsclass->input['password'];
		$homepage = $this->ipsclass->input['homepage'];
		$icq      = $this->ipsclass->input['icq_number'];
		$aim      = $this->ipsclass->input['aim_name'];
		$yahoo    = $this->ipsclass->input['yahoo'];
    $msn      = $this->ipsclass->input['msnname'];
		$salt     = $this->ipsclass->input['salt'];
    $time_offset=$this->ipsclass->input['time_offset'];
		$bday     = explode( "-", $this->ipsclass->input['birthday'] );
    $members_display_name = $this->ipsclass->input['members_display_name'];
    
    $salt = str_replace( "&#36;", "$", $salt );
    $salt = str_replace( "&#39;", "$", $salt );
    
		$salt = str_replace( "&#036;", "$", $salt );
		
		if ( !$name )
		{
			$errors[] = 'Name must be provided';
		}	
		
		if( $errors )
		{
			$this->return_error( $errors );
		}
		
		/* Check if id exists */
		$this->ipsclass->DB->simple_construct( array( 	'select' 	=> '*',
										 				'from'   	=> 'members',
							             				'where'		=> "name='".$name."'",
											));



		$this->ipsclass->DB->exec_query();
				
		$this->member = $this->ipsclass->DB->fetch_row();
		
		/* Found user? */			
		if ( ! $this->member['id'] )
		{
    		$errors[] = 'User does not exist';
		}
		
		if( $errors )
		{
			$this->return_error( $errors );
		}
		
		$id = $this->member['id'];

		/* Updating the password? */
		if( $salt && $password )
		{
			/* Check Salt */
			if( strlen( $salt ) > 5 )
			{
				$this->return_error( array( "The salt is too long, it must be less than 6 characters" ) );	
			}
			
			/* Build the converge array */
			$converge = array( 
							    'converge_pass_hash' => $password,
							    'converge_pass_salt' => str_replace( '\\', "\\\\", $salt )
							 );
		  
			/* Update */
			$this->ipsclass->DB->do_update( 'members_converge', $converge, "converge_id=$id" );				
		}
		
		/* Build the member array */
		$member = array(
						 'bday_day'             => $bday[1],
						 'bday_month'           => $bday[0],
						 'bday_year'            => $bday[2],
             'members_display_name' => $members_display_name,
             'members_l_username'   => strtolower($name),
             'members_l_display_name'=>strtolower($members_display_name),
             'time_offset'          => $time_offset
					   );
    
		/* Update member table */
		$this->ipsclass->DB->do_update( 'members', $member, "id=$id" );
    
		/* Build the member extra array */
		$extra = array( 
						'aim_name'   => $aim,
						'icq_number' => $icq,
						'website'    => $homepage,
						'yahoo'      => $yahoo,
            'msnname'    => $msn
					);
		
		/* Update members extra table */
		$this->ipsclass->DB->do_update( 'member_extra', $extra, "id=$id" );
		
		$this->return_ok( INVISION_USER_UPDATE_OK );
	}
	
	/**
	 * post_handler::register_user()
	 * 
	 * Registers a new user in the local IPB database
	 *
	 * The following fields must be passed through the POST:
	 *
	 * - integer id       Must be unique
	 * - string  username Must be unique
	 * - string  password Must be in the format md5( md5( salt ) . md5( password ) )
	 * - string  email    Must be unique
	 * - string  salt     Must be less than 6 characters
	 * 
	 * The following fields are optional
	 *
	 * - integer usergroupid	 
	 * - string  homepage
	 * - string  icq
	 * - string  aim
	 * - string  yahoo
	 * - string  joindate Must be a unix timestamp
	 * - string  birthday Must be in the format M-D-YYYY
	 *
	 * @access public
	 * @return xml
	 **/	
	function register_user()
	{			
		/* Authenticate Request */
		$this->authenticate();

		/* Load language */
		$this->ipsclass->load_language( 'lang_register' );
		
		/* Get the post input */
		$username = $this->ipsclass->input['username'];
		$password = $this->ipsclass->input['password'];
    $members_l_username = strtolower($this->ipsclass->input['username']);
    $members_l_display_name = strtolower($this->ipsclass->input['members_display_name']);
		$email    = $this->ipsclass->input['email'];
		$homepage = $this->ipsclass->input['homepage'];
		$icq      = $this->ipsclass->input['icq'];
		$aim      = $this->ipsclass->input['aim'];
		$yahoo    = $this->ipsclass->input['yahoo'];
		$joindate = $this->ipsclass->input['joindate'];
		$salt     = $this->ipsclass->txt_UNhtmlspecialchars($this->ipsclass->input['salt']);
		$bday     = explode( "-", $this->ipsclass->input['birthday'] );
		$group    = intval( $this->ipsclass->input['usergroupid'] );
		$group    = ( $group ) ? $group : 3;

		$salt = str_replace( "&#036;", "$", $salt );
    $salt = str_replace( "&#36;", "$", $salt );
    $salt = str_replace( "&#39;", "$", $salt );
		/* Error Checking */
		$errors = array();
		
		if( $id )
		{
			$errors[] = 'ID Parameter is now auto_incremented. ID is no longer used.';			
		}

		if( ! $username )
		{
			$errors[] = 'Username Parameter is missing';			
		}

		if( ! $password )
		{
			$errors[] = 'Password Parameter is missing';			
		}

		if( ! $email )
		{
			$errors[] = 'E-Mail Parameter is missing';			
		}
		
		if( ! $salt )
		{
			$errors[] = 'Salt Parameter is missing';			
		}

		if( strlen( $salt ) > 5 )
		{
			$errors[] = "The salt is too long, it must be less than 6 characters";
		}
		
		/* USERNAME: Is this name already taken? */
		$this->ipsclass->DB->cache_add_query( 'login_getmember', array( 'username' => strtolower($username) ) );
		$this->ipsclass->DB->cache_exec_query();

		$name_check = $this->ipsclass->DB->fetch_row();
			
		if ( $name_check['id'] )
		{
			$errors[] = $this->ipsclass->lang['reg_error_username_taken'];
		}
		
		/* EMAIL: Is this email already taken? */
		if ( $this->ipsclass->converge->converge_check_for_member_by_email( $email ) == TRUE )
		{
			$errors[] = $this->ipsclass->lang['reg_error_email_taken'];
		}
		
		if( $errors )
		{
			$this->return_error( $errors );
		}
		   
		/* Build the member array */
		$member = array(
						 'id'                   => '',
						 'name'                 => $username,
						 'members_display_name' => $username,
						 'member_login_key'     => $this->ipsclass->converge->generate_auto_log_in_key(),
						 'email'                => $email,
						 'mgroup'               => 3,
						 'posts'                => 0,
						 'joined'               => $joindate,
						 'time_offset'          => 0,
						 'view_sigs'            => 1,
						 'email_pm'             => 1,
						 'view_img'             => 1,
						 'view_avs'             => 1,
						 'restrict_post'        => 0,
						 'view_pop'             => 1,
						 'msg_total'            => 0,
						 'new_msg'              => 0,
						 'coppa_user'           => 0,
						 'language'             => $this->ipsclass->vars['default_language'],
						 'members_auto_dst'     => 1,
						 'allow_admin_mails'    => 0,
						 'hide_email'           => 1,
             'members_l_username'        => $members_l_username,
             'members_l_display_name'=> $members_l_display_name,
						 'bday_day'             => $bday[1],
						 'bday_month'           => $bday[0],
						 'bday_year'            => $bday[2],
					   );
					   
		/* Build the converge array */
		$converge = array( 
							'converge_email'     => $email,
						  	'converge_joined'    => $joindate,
						    'converge_pass_hash' => $password,
						    'converge_pass_salt' => str_replace( '\\', "\\\\", $salt )
						 );		
    
		/* Insert Converge */
		$this->ipsclass->DB->do_insert( 'members_converge', $converge );
		
		$id = $this->ipsclass->DB->get_insert_id();
    if ( !is_numeric( $id ) )
    {
      $this->return_error( INVISION_REGISTER_ERROR );
    }
		$member['id'] = $id;
		
		/* Insert Member */
		$this->ipsclass->DB->force_data_type = array( 'name' => 'string' );
		$this->ipsclass->DB->do_insert( 'members', $member );
		
		/* Build the member extra array */
		$extra = array( 
						'id'         => $id, 
						'vdirs'      => 'in:Inbox|sent:Sent Items',
						'aim_name'   => $aim,
						'icq_number' => $icq,
						'website'    => $homepage,
						'yahoo'      => $yahoo,
					);
    
		$this->ipsclass->DB->do_insert( 'member_extra', $extra );					
		
		/* Done */
		$this->return_ok();
	}
	
	/**
	 * post_handler::return_error()
	 * 
	 * Sends back a xml document of errors
	 *
	 * @access private
	 * @param  array   $errors
	 * @return xml
	 **/		
	function return_error( $errors )
	{
		
    if(!is_array($errors)) {
      header ("Content-Type: text/html" );
      echo $errors;
    }
    else if( $this->ipsclass->input['return_type'] == 'xml' )
		{
			/* Begin XML */
			$return_xml  = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
			$return_xml .= "<response>\n";
			$return_xml .= "<completed>0</completed>\n";		
			$return_xml .= "<error_count>" . count( $errors ) . "</error_count>\n";
			$return_xml .= "<errors>\n";
			
			/* Loop through each error */
			foreach( $errors as $e )
			{
				$return_xml .= "<error>$e</error>\n";
			}
			
			/* Finish XML */
			$return_xml .= "</errors>\n";
			$return_xml .= "</response>\n";
			
			/* Send */
			header( "Content-Type: text/xml" );
			echo $return_xml;
		}
		else 
		{
			/* commenting out case as spec, return 1 or 0
      foreach( $errors as $e )
			{
				$return_error .= "<error>$e</error>\n";
			}
			header( "Content-Type: text/html" );
			echo $return_error;
      */

    }
		exit();
	}
	
	/**
	 * post_handler::return_ok()
	 * 
	 * Sends back a xml document
	 *
	 * @access private
	 * @return xml
	 **/	
	function return_ok($return_code = 1)
	{
		if( $this->ipsclass->input['return_type'] == 'xml' )
		{		
			/* XML */
			$return_xml  = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
			$return_xml .= "<response>\n";
			$return_xml .= "<completed>$return_code</completed>\n";
			$return_xml .= "</response>\n";
			
			/* Send */
			header( "Content-Type: text/xml" );
			echo $return_xml;
		}
		else 
		{
			header( "Content-Type: text/html" );
			echo $return_code;			
		}
		exit();
	}
  
  /**
   * @author Earnest Berry III <earnest.berry@gmail.com>
   * Returns an array about the user.
   */
  function user_stats() {
    /* Authenticate */
    $this->authenticate();
    //---------------------------
    // General member information
    //---------------------------
    /* Check if id exists */
    
    $this->ipsclass->DB->simple_construct( array(   'select'  => '*',
      'from'    => 'members',
      'where'   => "name='".$this->ipsclass->input['u']."'",
                      ));
    $this->ipsclass->DB->exec_query();
    $this->member = $this->ipsclass->DB->fetch_row();
    
    //---------------------------
    // Setup before searching forumes
    //---------------------------
    $this->ipsclass->input['lastdate'] = isset($this->ipsclass->input['lastdate']) ? intval($this->ipsclass->input['lastdate']) : 0;
    
    if ( isset($this->ipsclass->input['lastdate']) AND is_numeric( $this->ipsclass->input['lastdate'] ) )
    {
      $last_time = time() - $this->ipsclass->input['lastdate'];
      $this->ipsclass->input['nav']    = 'at';
    }
    else {
      $last_time = $this->member['last_visit'];
    }
    
    
    //--------------------
    // Get forums
    //--------------------
    $forums = array();
    $this->ipsclass->DB->simple_construct( array( 'select' => 'f.id',
                            'from'   => 'forums f',
                            'where'  => "f.parent_id > 0 AND status=1") );
    $this->ipsclass->DB->exec_query();
    while($row = $this->ipsclass->DB->fetch_row()) {
      $forums[] = $row['id'];
    }
    $forums = implode(',', $forums);
    
    /* Go gadget go! */
    //--------------------
    // Active topics
    // Note: In this we need to count the posts and not use the one in the topics table 
    // so that we have the number of new posts in that topic that are new specifically 
    // to this user.
    // TODO Perfomrance hit of having title in the group by clause 
    //--------------------
    $active_topics = array();
    
    $this->ipsclass->DB->simple_construct(array( 'select' => 't.tid, t.title as topic_title',
                            'from'   => array('topics' => 't'),
                            'add_join' => array(
                              0 => array(
                                'select' => 'COUNT(p.pid) as new_posts',
                                'type' => 'inner',
                                'from' => array('posts' => 'p'),
                                'where' => 't.tid=p.topic_id'
                              )
                            ),
                            'limit' => array(0, 10),
                            'where'  => "t.approved=1 AND t.state != 'link' AND t.forum_id IN($forums) AND p.post_date >= {$last_time}",
                            'group'  => "t.tid, t.title",
                            'order'  => "t.last_post DESC" ));
    
    $this->ipsclass->DB->exec_query();
    while($row = $this->ipsclass->DB->fetch_row()) {
      $active_topics[] = $row;
    }
    $output['active_topics'] = $active_topics;

    //--------------------
    // Active posts
    //--------------------
    $active_posts = array();
    
    $this->ipsclass->DB->simple_construct( 
      array( 'select' => 't.title, t.tid',
              'from'   => array('topics' => 't'),
              'add_join' => array(
                0 => array(
                  'select' => 'p.pid, p.author_id, p.author_name',
                  'type' => 'inner',
                  'from' => array('posts' => 'p'),
                  'where' => 't.tid = p.topic_id'
                )
              ),
              'limit' => array(0, 10),
              'where'  => "t.approved=1 AND t.state != 'link' AND t.forum_id IN($forums) AND p.post_date >= {$last_time}",
              'order'  => "p.post_date DESC" ) );
    $this->ipsclass->DB->exec_query();
    
    while($row = $this->ipsclass->DB->fetch_row()) {
      $active_posts[] = $row;
    }
    $output['active_posts'] = $active_posts;
    
    /* Fly baby fly! (send out results) */
    $output['member'] = $this->member;
    ob_end_clean();
    
    print serialize($output);
    exit;
    return;
  }
  
  /**
   * TODO Make ths a join query so you only
   * run one query.
   */
  function user_info() {
    /* Authenticate */
    $this->authenticate();

    //---------------------------
    // All member information
    //---------------------------
    $this->ipsclass->DB->simple_construct( 
      array(   
      'select'  => 'm.*',
      'from' => array('members' => 'm'),
      'add_join' => 
        array( 
          0 => array(
            'select' => 'mx.*, mc.*',
            'type' => 'inner',
            'from' => array('member_extra' => 'mx', 'members_converge' => 'mc'),
            'where' => 'mx.id=m.id AND mc.converge_id=m.id'
          )
       ),
      'where'   => "name = '" . $this->ipsclass->input['u'] . "'"
      )
    );
    $this->ipsclass->DB->exec_query();
    
    // Place into array for transport
    while($row = $this->ipsclass->DB->fetch_row()) {
      foreach($row as $col => $v) {
        $user_info[$col] = $v;
      }
    }
    
    /* Found user? */     
    if ( !is_numeric($user_info['id']) || $user_info['id'] <= 0 )
    {
        $this->return_error( INVISION_LOGIN_ERROR_USER_NOT_FOUND );
    }
    
    /* Auth user if need be */
    if( $this->ipsclass->input['auth_user'] == 1 ) {
      /* Check Password */
      if( $user_info['converge_pass_hash'] != $this->ipsclass->input['p'] )
      { error_log("Comparing: " . $user_info['converge_pass_hash'] . " with " . $this->ipsclass->input['p']);
          $this->return_error( INVISION_LOGIN_ERROR_BAD_PASSWORD );
      }
    }
    
    
    $output['user_info'] = $user_info;
    
    /* Fly baby fly! (send out results) */
    
    ob_end_clean();
    
    print serialize($output);
    exit;
    return;
  }
  
  function server_stats() {
    /* Server stats */
    $this->authenticate();
    
    //--------------------
    // Get forums
    //--------------------
    $forums = array();
    $this->ipsclass->DB->simple_construct( array( 'select' => 'f.id',
                            'from'   => 'forums f',
                            'where'  => "f.parent_id > 0 AND status=1") );
    $this->ipsclass->DB->exec_query();
    while($row = $this->ipsclass->DB->fetch_row()) {
      $forums[] = $row['id'];
    }
    $forums = implode(',', $forums);
    
    /* Active topics/threads */
    
    //---------------------------
    // Today
    //---------------------------
    $last_time = 86400; // 24 hours
    $active_topics = array();
    $this->ipsclass->DB->simple_construct( array( 'select' => 't.tid, t.title as topic_title, COUNT(p.pid) as new_posts',
                            'from'   => 'topics t INNER JOIN posts p ON t.tid = p.topic_id',
                            'where'  => "t.approved=1 AND t.state != 'link' AND t.forum_id IN($forums) AND p.post_date >= {$last_time}",
                            'group'  => "t.tid, t.title",
                            'order'  => "t.last_post DESC" ) );
    $this->ipsclass->DB->exec_query();
    while($row = $this->ipsclass->DB->fetch_row()) {
      $active_topics[] = $row;
    }
    $output['today'] = $active_topics;
    
    //---------------------------
    // 2 weeks
    //---------------------------
    $last_time = 86400 * 14; // 24 hours
    $active_topics = array();
    $this->ipsclass->DB->simple_construct( array( 'select' => 't.tid, t.title as topic_title, COUNT(p.pid) as new_posts',
                            'from'   => 'topics t INNER JOIN posts p ON t.tid = p.topic_id',
                            'where'  => "t.approved=1 AND t.state != 'link' AND t.forum_id IN($forums) AND p.post_date >= {$last_time}",
                            'group'  => "t.tid, t.title",
                            'order'  => "t.last_post DESC" ) );
    $this->ipsclass->DB->exec_query();
    while($row = $this->ipsclass->DB->fetch_row()) {
      $active_topics[] = $row;
    }
    $output['two_weeks'] = $active_topics;
    
    //---------------------------
    // Month
    //---------------------------
    $last_time = 86400 * 31; // TODO Make this more sophisticated so that we get the exact days in the month, e.b. 28, 30, 31, 27
    $active_topics = array();
    $this->ipsclass->DB->simple_construct( array( 'select' => 't.tid, t.title as topic_title, COUNT(p.pid) as new_posts',
                            'from'   => 'topics t INNER JOIN posts p ON t.tid = p.topic_id',
                            'where'  => "t.approved=1 AND t.state != 'link' AND t.forum_id IN($forums) AND p.post_date >= {$last_time}",
                            'group'  => "t.tid, t.title",
                            'order'  => "t.last_post DESC" ) );
    $this->ipsclass->DB->exec_query();
    while($row = $this->ipsclass->DB->fetch_row()) {
      $active_topics[] = $row;
    }
    $output['month'] = $active_topics;
    
    //---------------------------
    // Year
    //---------------------------
    $last_time = 86400 * 366; // 366 days becuase i don't want to have to worry about there's 365.5 days in a year
    $active_topics = array();
    $this->ipsclass->DB->simple_construct( array( 'select' => 't.tid, t.title as topic_title, COUNT(p.pid) as new_posts',
                            'from'   => 'topics t INNER JOIN posts p ON t.tid = p.topic_id',
                            'where'  => "t.approved=1 AND t.state != 'link' AND t.forum_id IN($forums) AND p.post_date >= {$last_time}",
                            'group'  => "t.tid, t.title",
                            'order'  => "t.last_post DESC" ) );
    $this->ipsclass->DB->exec_query();
    while($row = $this->ipsclass->DB->fetch_row()) {
      $active_topics[] = $row;
    }
    $output['year'] = $active_topics;
    
    
    
    /* Fly baby fly! (send out results) */
    $output['member'] = $this->member;
    ob_end_clean();
    
    print serialize($output);
    exit;
    return;
    
  }
}
