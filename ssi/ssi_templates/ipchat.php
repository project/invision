<?

/*
+--------------------------------------------------------------------------
|   Invision Power Board
|   ========================================
|   by Matthew Mecham
|   (c) 2004 Invision Power Services, Inc
|   http://www.invisionpower.com
|   ========================================
|   Web: http://www.invisionpower.com
+---------------------------------------------------------------------------
|
|   > IP Chat => IPB Bridge Script
|   > Script written by Matt Mecham
|   > Date started: 17th February 2003 (Updated: 1 July 2004)
|   > Updated for 2.1.x: Thu 6th October 2005
|
+--------------------------------------------------------------------------
*/


/**
* SIGMACHAT auth file.
*
* Set-up and load module to run
*
* @package	InvisionPowerBoard
* @author   Matt Mecham
* @version	2.1
*/

/**
* Script type
*
*/
define( 'IPB_THIS_SCRIPT', 'public' );
define( 'IPB_LOAD_SQL'   , 'queries' );

require_once( './init.php' );

//===========================================================================
// MAIN PROGRAM
//===========================================================================

$INFO = array();

//--------------------------------
// Load our classes
//--------------------------------

require_once ROOT_PATH   . "sources/ipsclass.php";
require_once KERNEL_PATH . "class_converge.php";
require_once ROOT_PATH   . "conf_global.php";

# Initiate super-class
$ipsclass       = new ipsclass();
$ipsclass->vars = $INFO;

//--------------------------------
// The clocks a' tickin'
//--------------------------------
		
$Debug = new Debug;
$Debug->startTimer();

//--------------------------------
// Load the DB driver and such
//--------------------------------

$ipsclass->init_db_connection();

//--------------------------------
//  Set up our vars
//--------------------------------

$ipsclass->parse_incoming();

//--------------------------------
//  Set converge
//--------------------------------

$ipsclass->converge = new class_converge( $ipsclass->DB );

//--------------------------------
// Start off the cache array
//--------------------------------

$ipsclass->cache_array = array('rss_calendar', 'rss_export','components','banfilters', 'settings', 'group_cache', 'systemvars', 'skin_id_cache', 'forum_cache', 'moderators', 'stats', 'languages');


//===========================================================================
// Get cache...
//===========================================================================

$ipsclass->init_load_cache( $ipsclass->cache_array );

//--------------------------------
//  Initialize the FUNC
//--------------------------------

$ipsclass->initiate_ipsclass();

//===========================================================================
// AUTHORIZE...
//===========================================================================

define( 'DENIED', 0 );
define( 'ACCESS', 1 );
define( 'ADMIN' , 2 );

$allowed_groups        = $ipsclass->vars['chat_admin_groups'];
$access_groups         = $ipsclass->vars['chat_access_groups'];
$autologin             = 0;
$allow_guest_access    = $ipsclass->vars['chat_allow_guest'] == 1 ? ACCESS : DENIED;

// Stupid PHP changing it's mind on HTTP args

$username  = $_GET['username']  != "" ? $_GET['username'] : $HTTP_GET_VARS['username'];
$password  = $_GET['password']  != "" ? $_GET['password'] : $HTTP_GET_VARS['password'];
$ip        = $_GET['ip']        != "" ? $_GET['ip']       : $HTTP_GET_VARS['ip'];

//----------------------------------------------
// Test for autologin.
//----------------------------------------------

if ( preg_match( "/^(?:[0-9a-z]){32}$/", $password ) )
{
	$autologin = 1;
}

//----------------------------------------------
// Remove URL encoding (%20, etc)
//----------------------------------------------

$username = $ipsclass->parse_clean_value(urldecode(trim($username)));
$password = $ipsclass->parse_clean_value(urldecode(trim($password)));
$ip       = $ipsclass->parse_clean_value(urldecode(trim($ip)));

//----------------------------------------------
// Main code
//----------------------------------------------

// Start off with the lowest accessibility

$output_int  = $allow_guest_access;
$output_name = "";

//------------------------------
// Attempt to find the user
//------------------------------

$ipsclass->DB->build_query( array(	'select' 	=> 'm.mgroup, m.name, m.members_display_name, m.id, c.*',
									'from'		=> array( 'members' => 'm' ),
									'where'		=> "m.members_l_display_name='".$ipsclass->DB->add_slashes(strtolower($username))."'",
									'limit'		=> array( 0, 1 ),
									'add_join'	=> array( 1 => array( 	'type' 	=> 'left',
																		'from'	=> array( 'members_converge' => 'c' ),
																		'where'	=> 'c.converge_email=m.email',
														)			)
							)		);
$ipsclass->DB->exec_query();

						
if ( ! $member = $ipsclass->DB->fetch_row() )
{
	die_nice();
	
	//-- script exits --//
}


if ( ! $member['id'] )
{
	// No member found - allow guest access?
	
	die_nice($allow_guest_access);
	
	//-- script exits --//
}

//------------------------------
// Update passy
//------------------------------

if ( ! $autologin )
{
	$md5_password = md5( md5( $member['converge_pass_salt'] ) . md5($password) );
}
else
{
	$md5_password = $password;
}

//------------------------------
// Check password - member exists
//------------------------------

if ( $password != "" )
{
	// Password was entered..
	
	if ( $md5_password != $member['converge_pass_hash'] )
	{
		// Password incorrect..
		
		die_nice();
		
		//-- script exits --//
	}
	else
	{
		$output_int = ACCESS;
	}
}
else
{
	// No password entered - die!
	// Do not allow guest access on reg. name
	
	die_nice();
		
	//-- script exits --//
}


//------------------------------
// Do we have any access?
//------------------------------


if ( ! preg_match( "/(^|,)".$member['mgroup']."(,|$)/", $access_groups ) )
{
	die_nice();
}

//------------------------------
// Do we have admin access?
//------------------------------

if ( preg_match( "/(^|,)".$member['mgroup']."(,|$)/", $allowed_groups ) )
{
	$output_int = ADMIN;
}


//------------------------------
// Spill the beans
//------------------------------

print "SCRAS^1.1\nAUTH^".$output_int."\nUID^".intval($member['id'])."\n";

exit();
	 
	 
function die_nice( $access=0 )
{
	// Simply error out silently, showing guest access only for the user
	$this->ipsclass->DB->close_db();
	print $access;
	exit();
}

?>